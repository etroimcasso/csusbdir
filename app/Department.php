<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    public $incrementing = false;
    
    public function building() {
        $this->belongsTo('building','id');
    }
}
