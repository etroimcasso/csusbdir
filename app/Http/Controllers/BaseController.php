<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Building;

class BaseController extends Controller
{
    //
    public function __construct() {
        $ALL_BLDG = App\Building::all();
        
        View::share('All_Buildings', $ALL_BLDG);
    }
}
