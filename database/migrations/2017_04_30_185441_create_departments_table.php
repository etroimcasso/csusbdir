<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->string('name')->unique();
            $table->string('id')->primary();
            $table->string('main_office');
            $table->integer('phone_number');
            $table->text('description')->nullable();
            $table->string('building');
            $table->timestamps();
            
            $table->foreign('building')->
                    references('id')->on('buildings')->
                    onDelete('cascade');
                    
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
