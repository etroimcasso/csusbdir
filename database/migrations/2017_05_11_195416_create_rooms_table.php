<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /* Room needs
            •ID - increment
            •Room Number - Int
            •Floor - Int
            •Building - FK
            */
        Schema::create('rooms', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('number');
            $table->string('name');
            $table->integer('floor_num');
            $table->string('building');
            $table->text('description')->nullable();
            $table->integer('phone');
            $table->boolean('is_poi');
            $table->foreign('building')->
                references('id')->on('buildings')->onDelete('cascade');
       });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
 public function down()
    {
         Schema::table('rooms', function (Blueprint $table) {
            $table->dropForeign('rooms_building_foreign');
         });
        Schema::dropIfExists('rooms');
    }
}
