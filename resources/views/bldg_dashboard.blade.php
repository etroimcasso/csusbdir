@extends('app')

@section('title')
    {{ $building[0]->name . " Dashboard" }}
@endsection

@section('custom_css')
    <link rel="stylesheet" href="/css/carousel-fade.css">
@endsection

@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="building_title">
            <h3>{{ $building[0]->name }}<br/>
            <small>{{ $building[0]->description }}</small>
            </h3>
            @component('ui/poi_list',['pois' => $pois]) @endcomponent
        </div>
        <br/>
        <div class="floor">
            <div class="floor_tabs">
                @component('ui/floor_tabs',['building' => $building, 'rooms' => $rooms]) @endcomponent
            </div>
        </div>
    </div>


@endsection
