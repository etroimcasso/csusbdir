@extends('app')

@section('title')
    CSUSB Map Directory
@endsection

@section('custom_css')
@endsection

@section('content')
    <div class="container">
        
        
    </div>
    <div class="container-fluid">
        <div class="row">
            @foreach ( $buildings as $bldg )
                @component('ui/bldg_card',['building' => $bldg]) @endcomponent
            @endforeach
        </div>
    </div>
@endsection
