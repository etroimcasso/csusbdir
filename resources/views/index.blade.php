@extends('app')

@section('title')
    @if ( !empty($link->building))
    Campus Overview
    @endif
@endsection

@section('content')
    <div class="link_list">
    <!-- This part of the code should show the campus ma p with a list to the side or the bottom
        displaying links to the different buildings on campus -->
        @if ( $links[0]->getTable() != 'pois')
            @component('ui/list', ['links' => $links]) @endcomponent
        @else
            @component('ui/list', ['links' => $links, 'building' => $building[0]]) @endcomponent
        @endif
        
        
        @foreach ($links as $link)
        
        @endforeach
    </div>
@endsection