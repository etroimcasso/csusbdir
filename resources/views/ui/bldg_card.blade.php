<div class="col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width:50%rem;">
      <img class="card-img-top img-fluid" src="storage/{{ "bldg_img/" . $building->id . ".jpg" }}" $ alt="Image of {{ $building->name }} Building">
      <div class="card-block">
        <h4 class="card-title">{{ $building->name }}</h3>
        <p class="card-text">{{ $building->description }}</p>
        {{-- <p><a href="{{ $building->id }}" class="btn btn-block btn-primary" role="button">View Building</a></p> --}}
      </div>
    </div>
    <br/>
</div>