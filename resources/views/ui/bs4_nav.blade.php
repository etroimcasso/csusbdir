<nav class="navbar sticky-top navbar-toggleable-md navbar-inverse bg-primary">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Test">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">CSUSBdir</a>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      @if (Request::is('/'))
        <li class="nav-item active"><a class="nav-link" href="#">
      @else
        <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">
      @endif
      Home</a>
      </li>
      <li class="nav-item">
          <a class="nav-link dropdown-toggle" href="#" id="navbarBuildingsDrownDownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Buildings
          </a>
          <div class="dropdown-menu" aria-labelledby"navbarBuildingsDrownDownMenuLink">
              @foreach(App\Building::orderBy('name')->get() as $bldg)
              @if (Request::is($bldg->id))
                <a class="active dropdown-item"
              @else
                <a class="dropdown-item"
              @endif
              href="{{ $bldg->id }}">{{ $bldg->name }}</a>
            @endforeach
          </div>
      </li>
    </ul>
  </div>
</nav>
</br>

