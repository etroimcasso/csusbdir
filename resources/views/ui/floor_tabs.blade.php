<ul class="nav nav-tabs">
  @for ($i = 0; $i < $building[0]->floors; $i++)
        @if ($i == 0)
            <li class="active">
        @else
            <li>
        @endif
         <a data-toggle="tab" href="#floor{{ ($i + 1) - $building[0]->floors_underground }}">Floor {{ ($i + 1) - $building[0]->floors_underground}}</a></li>
  @endfor
</ul>

<div class="tab-content">
  @for ($i = 0; $i < $building[0]->floors; $i++)
    @if ($i == 0)
        <div id="floor{{ ($i + 1) - $building[0]->floors_underground }}" class="tab-pane in active">
    @else
        <div id="floor{{ ($i + 1) - $building[0]->floors_underground }}" class="tab-pane ">
    @endif
            @component('ui/map_carousel',['rooms' => $rooms, 'floor' => ($i + 1) - $building[0]->floors_underground ,'building'=> $building ]) @endcomponent
      </div>
  @endfor
</div>
