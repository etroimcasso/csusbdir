<ul>
    @foreach ($links as $link) 
    <div class="list_item">
        @if ( $link->getTable() == 'buildings' )
                @component('ui/bldg_list-item',['building' => $link]) @endcomponent
            </br>
            @if ( !empty($link->description) ) 
                &nbsp; {{ $link->description }}
            @endif
        @elseif ($link->getTable() == 'rooms' )
                <a href="{{ "/storage/map_img/" . strtolower($link->building) . "/" . strtolower($link->building) . "_r" . $link->number . ".svg"}}"> {{ strtoupper($link->number) }}</a>
            
        @elseif ($link->getTable() == 'pois' )
              <a href="{{ "/" . strtolower($link->building) . "/r/" . substr($link->room,strlen($link->building)) }}"> {{ strtoupper($link->room) . " - " . $link->name }}</a>
            @if ( !empty($link->description))
                </br>
                &nbsp; {{ $link->description }}
            @endif
        @endif

    </div>
    @endforeach
</ul>