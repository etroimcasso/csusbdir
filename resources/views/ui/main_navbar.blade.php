<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-education"></span></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        @if (Request::is('/'))
          <li class="active"><a href="#">
        @else
          <li><a href="{{ route('home') }}">
        @endif
        Home</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Buildings
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            @foreach(App\Building::orderBy('name')->get() as $bldg)
              @if (Request::is($bldg->id))
                <li class="active">
              @else
                <li>
              @endif
              @component('ui/bldg_list-item', ['building' => $bldg]) @endcomponent</li>
            @endforeach
          </ul>
        </li>
        @if (Request::is('rooms'))
          <li class="active"><a href="#">Rooms</a></li>
        @else
         <!-- <li><a href="rooms">Rooms</a></li> -->
        @endif
        @if (Request::is('poi'))
          <li class="active"><a href="#">POIs</a></li>
        @else
          <!-- <li><a href="poi">POIs</a></li> -->
        @endif
      </ul>
      <ul class="nav navbar-nav navbar-right">
      </ul>
    </div>
  </div>
</nav>

