<div id="mapCarousel_f{{ $floor }}" class="carousel carousel-fade slide">
    &nbsp;
    <div id="Controls">
        @component('ui/room-floor_dropdown',['rooms' => $rooms, 'i' => $floor]) @endcomponent
    </div>
    <div class="carousel-inner">
        @foreach (App\Room::where('building', $building[0]->id)->where('floor_num', $floor)->orderBy('number','asc')->get() as $room)
            @if ($loop->first)
                <div class="item active">@component('ui/floor-img',['building' => $building, 'i' => $floor]) @endcomponent
                    <div class="carousel-caption"><span style="color:black; font-size:20px;">Floor {{$floor}}</span></div>
                </div>
                <div class="item">@component('ui/room-img',['building' => $building, 'i' => $floor, 'room' => $room]) @endcomponent
                    <div class="carousel-caption"><span style="color:black; font-size:20px;">{{$room->name}}</span></div>
                </div>
            @else
                <div class="item">
                    @component('ui/room-img',['building' => $building, 'i' => $floor, 'room' => $room]) @endcomponent
                    <div class="carousel-caption"><span style="color:black; font-size:20px;">{{$room->name}}</span></div>
                </div>
            @endif
        @endforeach
    </div>
</div>
