@foreach (poi->where('floor_num',$i + 1) as $room) 
<div class="list poi">
    <a href="{{ "/" . strtolower($link->building) . "/r/" . substr($link->room,strlen($link->building)) }}"> {{ strtoupper($link->room) . " - " . $link->name }}</a>
            @if ( !empty($link->description))
                </br>
                &nbsp; {{ $link->description }}
            @endif
</div>
@endforeach