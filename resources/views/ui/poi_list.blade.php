<button class="btn btn-s btn-block btn-info rounded"  data-toggle="collapse" data-target="#poi_list">Points of Interest <span class="badge">{{ count($pois) }}</span></button>
        <div id="poi_list" class="collapse">
            <div class="col-*-12">
                <div class="panel panel-info rounded">
                <!--    <div class="panel-heading"><span class="heading">Points of Interest</span></div> -->
                    <div class="panel-body">
                        <div class="list-group">
                            @foreach($pois as $poi)
                                <a class= "list-group-item" href="#">
                            <!--    {{ "/" . strtolower($poi->building) . "/r/" . $poi->number . substr($poi->number,strlen($poi->building)) }}"> -->
                                    <h4 class="list-group-item-heading">{{$poi->name}} <small>{{ strtoupper($poi->number) }}</small></h4>
                                        @if ( !empty($poi->description))
                                            &nbsp;{{ $poi->description }}
                                        @endif
                                    </p>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>