<div class="dropdown">
    <button class="btn btn-block btn-primary dropdown-toggle" type="button" id="roomMenu{{$i}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Rooms<span class="caret"></span>   
    </button>
    <ul class="dropdown-menu" aria-labelledby="roomMenu{{$i}}">
        @foreach ($rooms->where('floor_num',$i) as $room)
            <li><a data-target="#mapCarousel_f{{$i}}" data-slide-to="{{$loop->index + 1}}" href="#">{{ strtoupper($room->building) . strtoupper($room->number) . ": " . $room->name }}</a></li>
        @endforeach
    </ul>
</div>