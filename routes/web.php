<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Building;
use App\Room;
use App\Department;
use App\Employee;
use App\Poi;

//Currently redirects to /smsu
Route::get('/', function () {
    $buildings = App\Building::all()->sortBy('name');
    return view('home',['buildings' => $buildings]);
   // return Redirect::to('/smsu');
    
})->name('home');

Route::get('/about', function() {
    return view('about');
});

Route::get('/buildings', function() {
    $buildings = App\Building::all()->sortBy('name');
    return view('index',['links' => $buildings]);
});

Route::get('/{building}/poi', function($building) {
    $pois = App\Poi::where('building', strtolower($building))->orderBy('room','asc')->get();
    return view('index',['links' => $pois]);
});

Route::get('/{building}/rooms', function($building) {
    $rooms = App\Room::where('building',strtolower($building))->orderBy('number','asc')->get();
    return view('index',['links' => $rooms]); //This needs to render its page just for room information
});

Route::get('/{building}/r/{room}', function($building, $room) {
    $room = App\Room::where('building',strtolower($building))->where('number',$room)->get();
    return view('index',['links' => $room]); //This needs to render its page just for room information
});

Route::get('/{building}', function($building) {
    $db_building = App\Building::where('id', strtolower($building))->get();
    $rooms = App\Room::where('building', strtolower($building))->orderBy('number','asc')->get();
    $pois = $rooms->where('is_poi', 1);
    //$pois = App\Poi::where('building', strtolower($building))->orderBy('room','asc')->get();
    return view('bldg_dashboard',['rooms' => $rooms, 'pois' => $pois, 'building' => $db_building]);

});